package bsa.java.concurrency.config;

import bsa.java.concurrency.logger.WebLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 16.07.2020
 */
@Configuration
public class Logging implements WebMvcConfigurer {
//    @Bean
//    public CommonsRequestLoggingFilter logFilter() {
//        CommonsRequestLoggingFilter filter
//                = new CommonsRequestLoggingFilter();
//        filter.setIncludeQueryString(true);
//        filter.setIncludePayload(true);
//        filter.setMaxPayloadLength(10000);
//        filter.setIncludeHeaders(false);
//        //filter.
//        filter.setAfterMessagePrefix("REQUEST DATA : ");
//        return filter;
//    }

    @Autowired
    private WebLogger logger;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(logger);
    }
}
