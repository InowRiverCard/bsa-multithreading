package bsa.java.concurrency.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 16.07.2020
 */
@Configuration
public class Executors {
    @Bean(name = "fileSystemExecutor")
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(10);
        executor.setMaxPoolSize(10);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("FileSystem-");
        executor.initialize();
        return executor;
    }

    @Bean(name = "imageServiceExecutor")
    public Executor imageServiceExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(10);
        executor.setMaxPoolSize(10);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("ImageService-");
        executor.initialize();
        return executor;
    }
}
