package bsa.java.concurrency.utilit;

import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 14.07.2020
 */
@Service
public class DHasher {

    public long calculateHash(byte[] image) {
        try {
            var img = ImageIO.read(new ByteArrayInputStream(image));
            return calculateDHash(preprocessImage(img));
        } catch (Exception err) {
            throw new RuntimeException(err.getMessage());
        }
    }

    private static BufferedImage preprocessImage(BufferedImage image) {
        var result = image.getScaledInstance(8, 8, Image.SCALE_SMOOTH);
        var output = new BufferedImage(8, 8, BufferedImage.TYPE_BYTE_GRAY);
        output.getGraphics().drawImage(result, 0, 0, null);

        return output;
    }

    private static int brightnessScore(int rgb) {
        return rgb & 0b11111111;
    }

    public static long calculateDHash(BufferedImage processedImage) {
        long hash = 0;
        for (var row = 1; row < 8; row++) {
            for (var col = 1; col < 8; col++) {
                var current = brightnessScore(processedImage.getRGB(row, col));
                var next = brightnessScore(processedImage.getRGB((row + 1) % 8, (col + 1) % 8 ));
                hash |= current > next ? 1 : 0;
                hash <<= 1;
            }
        }
        return hash;
    }

}
