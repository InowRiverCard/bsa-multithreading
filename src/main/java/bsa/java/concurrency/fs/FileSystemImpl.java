package bsa.java.concurrency.fs;


import bsa.java.concurrency.image.dto.ImageDTO;
import bsa.java.concurrency.image.exception.ImageNotFoundException;
import bsa.java.concurrency.image.model.ImageEntity;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;


import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 14.07.2020
 */
@Service
public class FileSystemImpl implements FileSystem {
    private final Logger logger = LoggerFactory.getLogger(FileSystemImpl.class);

    public static final String IMAGE_FOLDER = "upload" + File.separator + "images";

    @Value("${image.path}")
    public String localhost;

    @PostConstruct
    private void init() throws IOException {
        Files.createDirectories(Paths.get(IMAGE_FOLDER));
    }

    private String getExtension(String fileName) {
        var dotIndex = fileName.lastIndexOf(".");
        return dotIndex == -1 ? "" : fileName.substring(dotIndex);
    }


    @Async("fileSystemExecutor")
    @Override
    public CompletableFuture<ImageEntity> saveFile(ImageDTO file) {
        var id = this.generateId();
        var fileName = id.toString() + this.getExtension(file.getName());
        Path filePath = Paths.get(IMAGE_FOLDER, fileName);
        var url = this.getUrl(fileName);
        try {
            Files.write(filePath, file.getFile());
        } catch (IOException e) {
            logger.warn(e.getMessage(), e);
        }
        return CompletableFuture.completedFuture(new ImageEntity(id, url));
    }

    private UUID generateId() {
        return UUID.randomUUID();
    }

    @Override
    public void deleteById(UUID id) {
        var folder = new File(IMAGE_FOLDER);
        var files = folder.listFiles();
        if (files != null && files.length > 0) {
            for (File file : files) {
                var fileId = getId(file.getName());
                if (id.toString().equals(fileId)){
                    file.delete();
                    return;
                }
            }
        }
        throw new ImageNotFoundException("Image with such id does not exist. id=" + id);
    }

    @Override
    public void deleteAll() {
        var folder = new File(IMAGE_FOLDER);
        try {
            FileUtils.cleanDirectory(folder);
        } catch (IOException e) {
            logger.warn(e.getMessage(), e);
        }
    }

    @Override
    public File getFileByName(String name) {
        var path = IMAGE_FOLDER + File.separator + name;
        return new File(path);
    }

    private static String getId(String fileName) {
        var dotIndex = fileName.lastIndexOf(".");
        return dotIndex == -1
                ? fileName
                : fileName.substring(0, dotIndex);
    }

    private String getUrl(String fileName) {
        return localhost + fileName.replaceAll("\\\\", "/");
    }
}
