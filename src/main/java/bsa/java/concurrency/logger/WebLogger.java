package bsa.java.concurrency.logger;

import bsa.java.concurrency.image.ImageController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Date;

import static java.util.stream.Collectors.joining;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 16.07.2020
 */
@Component
public class WebLogger extends HandlerInterceptorAdapter {

    private final Logger logger = LoggerFactory.getLogger(ImageController.class);

    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler) {
        HttpServletRequest requestWrapper
                = new ContentCachingRequestWrapper(request);

        var params = requestWrapper.getParameterMap()
                .entrySet()
                .stream()
                .map(e -> e.getKey()+"="+ Arrays.toString(e.getValue()))
                .collect(joining("&"));
        Date date=java.util.Calendar.getInstance().getTime();
        var url = requestWrapper.getRequestURI();
        var method = requestWrapper.getMethod();
        logger.info(String.format("Date: %s; Method:%s; URL:%s; Params: %s", date, method, url, params));

        return true;
    }

    @Override
    public void afterCompletion(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler,
            Exception ex) {
        HttpServletResponse responseWrap = new ContentCachingResponseWrapper(response);
        var st = responseWrap.getStatus();
        var exceptionMsg = ex == null ? "" : " " + ex.getMessage();
        logger.info("Answer Status: " + st  + exceptionMsg);

    }
}
