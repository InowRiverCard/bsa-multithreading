package bsa.java.concurrency.validator;

import bsa.java.concurrency.image.exception.ImageValidationException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 16.07.2020
 */
@Service
public class Validator {

    private Optional<String> validateImage(MultipartFile file) {

        if (file.isEmpty()) {
            return Optional.of("file is empty " + file.getOriginalFilename());
        }

        var contentType = file.getContentType();
        if (contentType == null || !contentType.startsWith("image/")) {
            return Optional.of("File type mast be image " + file.getOriginalFilename());
        }

        return Optional.empty();
    }

    public void validSearchRequest(MultipartFile images, double threshold) {
        var errorMsg = validateImage(images);
        if (errorMsg.isPresent()) {
            throw new ImageValidationException(errorMsg.get());
        }

        if (threshold < 0 || threshold > 1) {
            throw new ImageValidationException("Threshold mast be within 1");
        }

    }

    public void validateImages(MultipartFile[] images) {
        for (MultipartFile image: images) {
            var errorMsg = validateImage(image);
            if (errorMsg.isPresent()){
                throw new ImageValidationException(errorMsg.get());
            }
        }
    }
}
