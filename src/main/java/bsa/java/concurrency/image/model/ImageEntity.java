package bsa.java.concurrency.image.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 14.07.2020
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "images")
public class ImageEntity {
    @Id
    private UUID id;

    @Column
    private Long hash;

    @Column
    private String url;

    public ImageEntity(UUID id, String url) {
        this.id = id;
        this.url = url;
    }
}
