package bsa.java.concurrency.image.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 15.07.2020
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ImageDTO {
    private String name;
    private byte[] file;

    @SneakyThrows
    public static ImageDTO fromMultipart(MultipartFile file) {
        return new ImageDTO(file.getOriginalFilename(), file.getBytes());
    }
}
