package bsa.java.concurrency.image.exception;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 16.07.2020
 */
public class ImageNotFoundException extends RuntimeException {
    public ImageNotFoundException(String msg) {
        super(msg);
    }
}
