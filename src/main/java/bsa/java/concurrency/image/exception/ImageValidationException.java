package bsa.java.concurrency.image.exception;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 16.07.2020
 */
public class ImageValidationException extends RuntimeException{
    public ImageValidationException(String msg) {
        super(msg);
    }
}
