package bsa.java.concurrency.image;

import bsa.java.concurrency.image.model.ImageEntity;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 14.07.2020
 */
public interface ImageRepository extends JpaRepository<ImageEntity, UUID> {

    @Query(value = "select CAST(i.id as varchar) imageId, matchPercent, i.url imageUrl " +
                   "from images as i, hamming_dist(i.hash, :hash) as matchPercent " +
                   "where matchPercent >= :percent ",
            nativeQuery = true)
    List<SearchResultDTO> findByHammingDist(long hash, double percent);
}
