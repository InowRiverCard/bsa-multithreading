package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.ImageDTO;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.exception.ImageNotFoundException;
import bsa.java.concurrency.image.exception.ImageValidationException;
import bsa.java.concurrency.validator.Validator;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/image")
public class ImageController {

    final private ImageService imageService;

    private final Validator validator;

    public ImageController(ImageService imageService, Validator validator) {
        this.imageService = imageService;
        this.validator = validator;
    }

    @PostMapping("/batch")
    @ResponseStatus(HttpStatus.CREATED)
    public void batchUploadImages(@RequestParam("images") MultipartFile[] files) {
        this.validator.validateImages(files);
        var images = Arrays.stream(files)
                .map(ImageDTO::fromMultipart)
                .collect(Collectors.toList());
        this.imageService.SaveImages(images);
    }

    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<SearchResultDTO> searchMatches(@RequestParam("image") MultipartFile file,
                                               @RequestParam(value = "threshold", defaultValue = "0.9") double threshold) {
        this.validator.validSearchRequest(file, threshold);
        var imageDto = ImageDTO.fromMultipart(file);
        return this.imageService.findByHammingDist(imageDto, threshold);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteImage(@PathVariable("id") UUID imageId) {
        this.imageService.deleteImgById(imageId);
    }

    @DeleteMapping("/purge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void purgeImages(){
        this.imageService.deleteAll();
    }


    @GetMapping(value = "/{fileName:.+}")
    public void getGifFromUsers(HttpServletResponse response,
                                @PathVariable String fileName) throws IOException {
        var file = this.imageService.getFileByName(fileName);
        if (!file.exists()) {
            throw new ImageNotFoundException("Image does not exist name=" + fileName);
        }
        InputStream in = new FileInputStream(file);
        IOUtils.copy(in, response.getOutputStream());
    }


    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({ ImageNotFoundException.class })
    public String handleNotFoundException(ImageNotFoundException ex) {
        return ex.getMessage();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ ImageValidationException.class })
    public String handleValidationException(Exception ex) {
        return ex.getMessage();
    }
}
