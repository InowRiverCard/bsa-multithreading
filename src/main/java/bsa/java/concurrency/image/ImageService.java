package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.ImageDTO;
import bsa.java.concurrency.image.model.ImageEntity;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.utilit.DHasher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 14.07.2020
 */
@Service
public class ImageService {
    @Autowired
    FileSystem fileSystem;

    @Autowired
    ImageRepository imageRepository;

    @Qualifier("imageServiceExecutor")
    @Autowired
    Executor executor;

    @Autowired
    DHasher hasher;

    public void SaveImages(List<ImageDTO> images) {
        var hashes =  images.parallelStream()
                .map(this::HandleImg)
                .collect(Collectors.toList());
        this.imageRepository.saveAll(hashes);

    }

    private ImageEntity HandleImg(ImageDTO file) {
        var imgSaveResult = fileSystem.saveFile(file);    //async method
        var hash = hasher.calculateHash(file.getFile());
        var result = imgSaveResult.join();
        result.setHash(hash);
        return result;
    }

    public List<SearchResultDTO> findByHammingDist(ImageDTO image, double threshold) {
        var hash = hasher.calculateHash(image.getFile());
        var images = imageRepository.findByHammingDist(hash, threshold);
        if (images.isEmpty()) {
            executor.execute(() -> this.saveImg(image, hash));
        }
        return images;
    }

    private void saveImg(ImageDTO dto, Long hash) {
        var image = fileSystem.saveFile(dto).join();
        image.setHash(hash);
        imageRepository.save(image);
    }

    public File getFileByName(String name) {
        return fileSystem.getFileByName(name);
    }

    public void deleteImgById(UUID id) {
        fileSystem.deleteById(id);
        imageRepository.deleteById(id);
    }

    public void deleteAll() {
        fileSystem.deleteAll();
        imageRepository.deleteAll();
    }

}
