CREATE OR REPLACE FUNCTION hamming_dist(f bigint, s bigint) RETURNS double precision AS $$
DECLARE n integer;
    DECLARE i bigint;
    DECLARE amount double precision;
BEGIN
    i := f # s;
    amount := 0;
    FOR n IN 1..64 LOOP
            amount := amount + ((i >> (n-1)) & 1);
        END LOOP;
    RETURN (1 - (amount / 64));
END
$$ LANGUAGE plpgsql;